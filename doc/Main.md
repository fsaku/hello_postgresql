# Giuseppe Crino', 794281, 18 Aprile 2016
# "Food Express"

# Documentazione tecnica

## Schema ER della base di dati

```
.=======. N               M .=======. N           1 .======.
| plate |--< included_in >--| order |--< ordered >--| user |
'======='                   '======='               '======'
    |                           | 
    \-------< threaded_to >-----/
                 |
                 |
            .========. 
            | thread |
            '========'
```
```

Attributes:

.=======.
| plate |       : {
'======='             name, img_url, description, minutes_needed,
                      complexity, many, price
                  }

.=======.
| order |       : { id, address_to_deliver, requested_at, proposed_at }
'======='

.======.
| user |        : { username, password }
'======'

.========.
| thread |      : { thread_id }
'========'

< ordered >     : { last_name, phone_number, ssn, vat }

< threaded_to > : { started_at, proposed_at }

[ last_name, phone_number, ssn and vat are provided at the
time of the order. They're not meant to be fixed for a user ]
```

## Schema relazionale della base di dati

```
plate
    (**id**, name, img_url, description, minutes_needed, complexity)

plate_available
    (__plate__, many, price)

user
    (**id**)

auth_data (__user__, sha1sum, auth_token)

order
    (
        **id**, __user__, last_name, phone_number, ssn, vat,
        address_to_deliver, requested_at, proposed_at
    )

plate_ordered (__plate__, __order__, proposed_at, started_at, thread)

[ **primary_key**, __foreign_key__ ]
```

## Architettura dell'applicazione

Ho scelto di strutturare l'applicazione usando
un _middleware_ RESTful che comunica direttamente
con il database, e un frontend
restituito ai client, che consulta
e aggiorna il database via HTTP.

Questo tipo di scelta mi ha permesso di programmare il client
slegandomi dalle logiche del modello relazionale,
preoccupandomi dei soli dati e del formato in questi
fossero codificati.

L'autenticazione e la gestione dei permessi dei client
avviene mostrando al server 
un token di 256bit, generato server-side nel momento
in cui il client e' in grado di fornire una coppia
valida `username:sha1sum(password)`.

Nello specifico, il database relazionale e' mantenuto
da `PostgreSQL`; il middleware e' scritto usando la
libreria `Express` per `Node.js` e il driver per postgres
`node-postgres` di `@brianc`. Il front-end
e' una tradizionale combinazione di `HTML+CSS+Javascript`.

Front-end e middleware  comunicano fra di loro codificando
e decodificando i dati usando `JSON`.

Il database contiene poi una parte di logica, implementata
in `python2.7`, per tenere lo
stato della base di dati consistente, e per rendere atomiche
una serie di operazioni di insertimento -- spostarle nel
middleware avrebbe reso meno robusto il codice.

La _codebase_ e' illustrata di seguito

```
% tree -L 3 -I 'node_modules|doc|root' --charset=ascii
.
|-- etc
|   |-- createdb -> /usr/bin/createdb
|   |-- create_tables.sql
|   |-- dropdb -> /usr/bin/dropdb
|   |-- running.dump.sql
|   |-- functions.sql
|   |-- populate_tables.sql
|   `-- triggers.sql
|-- index.js
|-- lib
|   |-- parse_cookie.js
|   `-- random_auth_token.js
|-- package.json
`-- src
    |-- routers
    |   |-- api.js
    |   |-- root.js
    |   `-- utility.js
    |-- server.js
    `-- views
        |-- admin.html
        |-- auth.html
        |-- change_passwd.html
        |-- style.css
        |-- user.html
        `-- utility.js

5 directories, 20 files
```

## Funzioni realizzate 

### Gestione degli errori

Il middleware cerca di non andare mai in crash,
ma di tornare immediatamente operativo appena si rende
conto che o la richiesta non puo'
essere soddisfatta, o nel database e' presente
uno stato diverso da quello che atteso 
(_e.g._ esistono due piatti diversi con lo stesso id,
`db.inconsistency`).

In tal caso, prima di chiudere la socket,
manda un errore formattato come

```
{
    error: <string>
}
```

Dato che il campo `error` non e' mai restituito in caso
di successo, il client puo' controllare quindi
se `res.body.error !== null`, deducendo che qualcosa e' andato storto server-side.

### `GET /api/whoami`

In base all'`auth_token` passato in `Cookie` al server,
viene restituito il nome utente di chi sta facendo
la richiesta -- consultando l'indice su `auth_token`
e non con una _full-scan_ della relazione `auth_data`.

### `GET /api/plate`

Restituisce un oggetto della forma

```
{
    plates: [
        {
            id: <integer>,
            name: <string>,
            img_url: <string>,
            description: <string>,
            minutes_needed: <integer>,
            complexity: <integer in [1,5]>,
            many: <integer>,
            price: <double>
        },
        ...
    ]
}
```

Internamente, il middleware fa al database una query
del tipo

```
select p.*, at.many, at.price
from plate as p
left outer join plate_available as at
on p.id=at.plate
```

### `POST /api/plate`

Disponibile solo per l'utente `admin`. Se il
token passato in `Cookie:` non e' quello dell'amministratore,
la richiesta viene al piu' presto rifiutata restituendo un
`{ error: "middleware.permission.denied" }`.

Il middleware si aspetta di ricevere un oggetto
della forma

```
{
    plate: {
        name: <string>,
        img_url: <string>,
        description: <string>,
        minutes_needed: <integer>,
        complexity: <integer in [1,5]>
    }
}
```

Il middleware tenta _ottimisticamente_ di inserire
il piatto; se viene violato qualche vincolo interno
al database (_e.g._ esiste gia' un piatto con
quel nome), la richiesta viene rifiutata
e viene restituito al client un `{ error: "db.query.failed" }`.

### `POST /api/plate_available`

Disponibile solo per l'`admin` -- decorando
l'handler della richiesta con `as_admin_only()`.

Il middleware si aspetta un

```
{
    plate: {
        id: <integer>,
        many: <integer>,
        price: <double>
    }
}
```

ed effettua una

```
update plate_available
set many=p.many, price=p.price
where plate=p.id
```

### `POST /api/order`

Riceve un oggetto del tipo

```
{
    id: <string>,
    last_name: <string>,
    phone_number: <string>,
    ssn: <string>,
    vat: <string>,
    requested_at: <string, ISO 8601>,
    address_to_deliver: <string>,
    plates: <integer[]>,
}
```

Quindi recupera l'utente che sta effettuando la richiesta
tramite `auth_token` -- non e' possibile farne una
senza un token valido (`middleware.permission.denied`)
-- quindi viene
chiamata la funzione `insert_order()`, che prende
un oggetto strutturato come

```
struct {
    id char[],
    user char[],
    last_name char[],
    phone_number char[],
    ssn char[],
    vat char[],
    requested_at char[],
    address_to_deliver char[],
    plates int32_t[]
};
```

e **in un'unica transazione**, inserisce il nuovo ordine
nella relazione `order`, inserisce i piatti `plates`
nella relazione `plate_ordered`, e infine richiama
la funzione `enqueue_plates()` che va ad inserire
nelle diverse linee di preparazione ciascun piatto dell'ordine.

La scelta del set dei lavoratori avviene privilegiando
quelli piu' veloci nella preparazione dell'ordine `id`.

Si cerca di tenere lo stato del database coerente sia come
detto sopra con l'uso di un'unica transazione, che con una
serie di trigger _ad-hoc_.

Ad esempio

```
create trigger plate_available
    before insert on plate_ordered
    for each row execute procedure plate_available();
```

oppure

```
create trigger update_availability
    after insert or delete on plate_ordered
    for each row execute procedure update_availability();
```

L'oggetto restituito e' della forma

```
{
    id: <string>,
    requested_at: <string ISO 8601>,
    proposed_at: <string ISO 8601>
}
```

### `GET /api/order/by/:user`

Restituisce semplicemente il risultato di

```
select _order, last_name, o.proposed_at, name
from _order as o
join plate_ordered as po on po._order=o.id
join plate as p on p.id=po.plate
where _user=:user
```

### `GET /api/stats`

Ciclando `i` sul numero di lavoratori del
ristorante, viene chiamata la funzione `compute_stats_for_thread(i)`
che restituisce un oggetto del tipo

```
{
    id: <integer>,
    nplates_prepared: <integer>,
    working_minutes: <double>,
    user_served: <integer>,
    average_value_of_plates: <double>
}
```

Il middleware restituisce infine una lista di oggetti `stat_t`,
del formato appena descritto.

### `GET /api/meta`

Il middleware restituisce un oggetto del tipo

```
{
    nthreads: <integer>,
    max_minutes_to_deliver: <integer>
}
```

### `POST /api/meta`

Disponibile solo per l'`admin`, accetta

```
{
    nthreads: <integer>,
    max_minutes_to_deliver: <integer>
}
```

### enqueue_plates()

```
create or replace function
    enqueue_plates(order_id text) returns timestamp with time zone as $$
        from dateutil.parser import parse
        import datetime
        
        class UTC(datetime.tzinfo):
            def utcoffset(self, dt):
                return datetime.timedelta(0)
            def tzname(self, dt):
                return 'UTC'
            def dst(self, dt):
                return datetime.timedelta(0)
        
        def fetch_nthreads():
            query = 'select * from nthreads'
            return plpy.execute(query)[0]['nthreads']

        def fetch_plate_ids_by_order(id):
            query = '''
                select * 
                from plate_ordered as po, _order as o
                where po._order=o.id and o.id=$1
            '''
            plan = plpy.prepare(query, ['text'])
            return map(lambda x: x['plate'], plpy.execute(plan, [id]))

        def fetch_minutes_needed_for(p_id):
            query = 'select * from plate where id=$1'
            res = plpy.execute(plpy.prepare(query, ['integer']), [p_id])
            return datetime.timedelta(0, int(res[0]['minutes_needed']) * 60)

        def fetch_last_proposed_time_by(t_id):
            fmt = 'select * from thread{} order by proposed_at desc'
            query = fmt.format(t_id)

            res = plpy.execute(query)
            fn = lambda r: \
                   parse(r['proposed_at']) > datetime.datetime.now(tz=UTC())
            res = filter(fn, res)

            if len(res) == 0:
                return datetime.datetime.now(tz=UTC())
           
            return \
                parse(res[0]['proposed_at'])

        def fetch_best_thread_id_for(p_id):
            lst = []
            min_needed = fetch_minutes_needed_for(p_id)
            n_threads = fetch_nthreads()

            for t_id in range(n_threads):
                tstamp = fetch_last_proposed_time_by(t_id)
                lst.append((t_id, tstamp + min_needed))

            lst.sort(key=lambda x: x[1])
            return lst[0]

        def enqueue(t_id, p_id, proposed_at):
            # Take a deep breath!, and ...
            query = '''
                update plate_ordered
                set thread=$1
                where _order=$2 and plate=$3
            '''
            plan = plpy.prepare(plan, ['integer', 'text', 'integer'])
            plpy.execute(plan, [t_id, order_id, p_id])

            query = '''
                update plate_ordered
                set proposed_at=$1
                where _order=$2 and plate=$3
            '''
            plan = plpy.prepare(plan, [
                      'timestamp with time zone', 'text', 'integer'
                   ]
            plpy.execute(plan, [
               proposed_at.isoformat(), order_id, p_id
            ])

            rv = plpy.execute('''
                select * from thread{}
                order by proposed_at desc
            '''.format(t_id))
            fn = lambda r: \
                    parse(r['proposed_at']) > datetime.datetime.now(tz=UTC())
            rv = filter(fn, rv) 

            if len(rv) > 0:
                started_at = rv[0]['proposed_at']
            else:
                started_at = datetime.datetime.now(tz=UTC()).isoformat()

            query = '''
                update plate_ordered
                set started_at=$1
                where _order=$2 and plate=$3
            ''',
            plan = plpy.prepare(plan, [
               'timestamp with time zone', 'text', 'integer'
            ]
            plpy.execute(plpy.prepare(plan, [
               started_at, order_id, p_id
            ])

            fmt = 'refresh materialized view thread{}'
            plpy.execute(fmt.format(t_id))

        lst = []

        plate_ids = fetch_plate_ids_by_order(order_id)

        if fetch_nthreads() < 1: raise plpy.Error('No thread available')
        if plate_ids < 1: return None

        for p_id in plate_ids:
            t_id, proposed_at = fetch_best_thread_id_for(p_id)

            enqueue(t_id, p_id, proposed_at)

            tstamp = fetch_last_proposed_time_by(t_id)
            lst.append(tstamp)

        ret = sorted(lst)[-1]

        ## Fetch time needed to deliver ...
        query = 'select address_to_deliver from _order where id=$1'
        rv = plpy.execute(plpy.prepare(query, ['text']), [order_id])
        destination = rv[0]['address_to_deliver']

        import requests, json

        url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
        url += '?key={}'.format('AIzaSyA3CC9oZCepcSTpq-ens5zTr050VWLAfRU')
        url += '&origins={}'.format( \
                  '+'.join('Via Comelico, Milano'.split(' '))
               )
        url += '&destinations={}'.format('+'.join(destination.split(' ')))

        r = requests.get(url)

        if r.status_code != 200:
            raise plpy.Error('Something went wrong with Google Maps API')

        body = json.loads(r.text)

        if body['status'] != 'OK':
            raise plpy.Error('Something went wrong with Google Maps API')

        seconds_needed = body['rows'][0]['elements'][0]['duration']['value']
        try:
            max_seconds_to_deliver = \
               plpy.execute('''
                   select * from max_minutes_to_deliver
               ''')[0]['max_minutes_to_deliver'] * 60
            max_seconds_to_deliver = \
               datetime.timedelta(0, max_seconds_to_deliver)
        except Exception:
            max_seconds_to_deliver = None

        seconds_needed = datetime.timedelta(0, seconds_needed)

        if max_seconds_to_deliver and seconds_needed > max_seconds_to_deliver:
            msg = 'Order cannot be delivered due to admin constraint'
            raise plpy.Error(msg)

        ret += seconds_needed

        plpy.execute(plpy.prepare(
            'update _order set proposed_at=$1 where id=$2',
            ['timestamp with time zone', 'text']
        ), [ret.isoformat(), order_id])

        return ret.isoformat()
$$ language plpythonu;
```

### as_admin_only()

```
function
as_admin_only(fn) {
return function(req, res) {
    pg.connect(connection_string, function(err, client, done) {
        if (err) {
            done();
            return fail(new Error(`db.connection, ${err.message}`), res);
        }

        let auth_token = '';
        try {
            auth_token = parse_cookie(req.headers['cookie'])['auth_token'];
        } catch (e) {
            done();
            return fail(new Error('middleware.permission.denied'), res);
        }

        let fmt = 'select * from auth_data where auth_token=$1';
        let query = client.query(fmt, [auth_token]);

        query.on('error', function(err) {
            done();
            return fail(new Error(`db.query.failed, ${err.message}`), res);
        });

        let rows = [];

        query.on('row', function(row) {
            rows.push(row);
        });

        query.on('end', function() {
            if (rows.length > 1) {
                done();
                return fail(new Error(`db.inconsistency`), res);
            }

            if (rows.length === 1 && rows[0]['_user'] === 'admin') {
                done();
                return fn(req, res);
            }

            done();
            return fail(new Error('middleware.permission.denied'), res);
        });
    });
};
}
```

## Manuale utente

Per installare e lanciare l'applicazione e' possibile usare
i comandi che seguono

```
$ pip2 install requests
$ pip2 install python-dateutil
$ git clone https://fsaku@bitbucket.org/fsaku/hello_postgresql.git
$ cd hello_postgresql/
$ createdb test
$ psql -d test -f etc/create_tables.sql
$ psql -d test -f etc/functions.sql
$ psql -d test -f etc/triggers.sql
$ psql -d test -f etc/populate_tables
$ npm install
$ npm start

> food_express@ start /home/crusty/food_express
> node ./index.js

*** Listening on port 8080 ...
```

(e' disponibile un dump del database in funzione,
in `etc/running.dump.sql`)

A questo punto, aprendo un browser, andando a `localhost:8080`
viene presentata una schermata di login/registrazione.

Se si e' gia' utenti, per accedere ai propri dati, e' necessario
digitare la propria coppia `username:password`; se si e' nuovi
utenti, `username:password` digitati varranno come _sign-up_;
mentre le credenziali per l'admin sono `admin:admin`.

Internamente viene effettuata una `POST /api/auth` passando
al server

```
{ username: <string>, sha1sum: <string> }
```

dove `sha1sum` viene calcolata dal client *prima* di mandare
i dati al server.

In questo modo il server non conosce mai la password dell'utente.

Tecniche di *salting* non sono state previste.

Se tutto avviene correttamente, il server manda come risposta

`{ auth_token: string }`

e `Set-Cookie: auth_token=<auth_token>` fra gli header.

Mandare al client l' `auth_token` e' stato necessario,
dato che il meccanismo `Set-Cookie` non e' compatibile
con l'uso delle `AJAX`: e' necessario
settarli con `document.cookie`.

A questo punto, se l'utente e' `admin`, viene mostrata la
pagina dell'amministratore, altrimenti la pagina di un utente normale.

### Amministratore

Se si e' `admin`, viene mostrata una lista dei piatti
gia' inseriti all'interno del database, mappando client-side
il risultato di `GET /api/plate` in nodi della DOM.

Di ciascuno
e' possibile con un form cambiare disponibilita' (_Many?_)
e prezzo (_Price?_), oltre a poterne aggiungere di nuovi.

L'`admin` puo' poi cambiare il numero di lavoratori,
e il tempo massimo consentito per raggiungere la localita'
indicata dal cliente (se il cliente specifica una consegna
per un punto troppo lontano dal ristorante, l'ordine
non viene accettato).

Infine, viene visualizzata una serie di statistiche sui thread
nel formato

```
<pthread_t>, prepared <integer> plates, for <double> minutes,
serving <integer> clients, selling something like <double>
per shot.
```

### Utente normale

Se si e' utenti con permessi normali invece, la
pagina che viene mostrata permette principalmente 
di effettuare nuovi ordini.

Viene mostrata una lista dei piatti, un form
per specificare il cognome relativo all'ordine,
l'orario richiesto di consegna (non necessario),
l'indizzo a cui deve essere consegnato l'ordine, oltre
ad informazioni per la fatturazione (_ssn_ e _vat_),
e il numero di telefono.

Se l'utente clicca su `> Make a new order`,
un token di 64bit viene generato client-side
e viene usato come `order_id`.

Il set di piatti
scelto, assieme alle informazioni per l'ordine,
viene mandato al database.

A questo punto, il database calcola l'orario
in cui riesce a consegnare l'ordine. Se non e' entro
l'orario specificato dal client, viene mostrata una
richiesta di conferma: il client puo' annullare l'ordine
e proporre un nuovo orario.

Segue infine uno storico degli ordini precedenti
effettuati dal cliente, oltre ad un buttone per
il logout e uno per cambiare password.

---

## Considerazioni finali

L'applicazione e' solo un prototipo ed e' migliorabile
sotto molti punti di vista.

Sarebbe utile dotare l'API REST di una serie di test che
controllino almeno lo schema delle rappresentazioni
restituite. Anche la gestione degli errori andrebbe
migliorata: le richiesta falliscono senza mandare
in crash l'applicazione, eppure i messaggi di errore
non sono spesso abbastanza _evocativi_/chiari.

L'API REST e' ritagliata sul tipo di consumo
che ne fara' il client, mentre probabilmente
aver usato un design piu' _puro_, a discapito
del numero di request necessarie ad avere
una rappresentazione sufficiente del dato,
non avrebbe inciso sensibilmente sulle prestazioni,
ma avrebbe pagato con una API piu' chiara.

Il meccanismo dell'autenticazione con il token
andrebbe poi irrobustito aggiungendo una scadenza
al token -- in questo momento il token e' un informazione
legata all'utente e non ad una particolare sessione.

Probabilmente, affidarsi a meccanismi di autenticazioni
piu' assodati come i `JSON Web Token`, o sfruttare
le strutture di autenticazione di altri servizi -- con `OAuth2` --
sarebbe stato piu' saggio che _"reinventare la ruota"_.

Per quanto riguarda l'hashing delle password, la scelta
di usare `sha1` andrebbe rivista. Usare
una funzione di hashing cosi' _nota_ e veloce da calcolare
(per di piu', senza _salt_) e' pericoloso.

Usare un algoritmo di hash _CPU-intensive_ o _memory-intensive_,
come `bcrypt` o `scrypt`, sarebbe stato piu' opportuno.

La scelta di usare Javascript server-side andrebbe poi
riconsiderata.

Il meccanismo a thread singolo, _non-blocking I/O_,
permette di ottenere con pochissimo sforzo
un server fluido che non _blocchi_; il debito
da pagare pero' e' che programmare l'event loop significa 
scrivere moltissimo codice _boilerplate_: per la gestione degli
errori, per l'arrivo di un chunk del risultato, 
l'arrivo del risultato completo, ...

Alla lunga questo tipo di programmazione diventa troppo ripetitiva,
poco stimolante, e quindi _error-prone_!

Che poi esista _underground_ una cultura di _best practices_
volta a ridurre queste problematiche e' un altro conto.

Solamente per gestire una query bisogna scrivere
qualcosa come

```
let query = client.query('select * from table');

query.on('error', function(err) {
    // Handle error
});

query.on('row', function(row) {
    // Handle row
});

query.on('end', function() {
    // Handle end
});
```

Usare un linguaggio come Go, con paradigma di programmazione
sincrona ma con _coroutines_,
sarebbe stato ingegneristicamente piu' felice.
