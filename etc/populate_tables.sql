begin;
insert into plate(name, minutes_needed) values
    ('Lasagne alla Morricone', 45),
    ('Puntarelle alla romana', 10),
    ('Spaghetti di Mario Brega', 20),
    ('Caprese con pasta d''acciughe', 5),
    ('Ossobuco al pesto', 50),
    ('Pollo mele, limone e finocchi', 10);

insert into _user values
    ('mean'), ('meow'), ('qwerty'),
    ('stack_overflow'), ('admin');

insert into auth_data values
    (
        'mean',
        '5f00f7a0a9f5e57deb39805aa521121fa277e75e',
        'd55564d563206dad63f4eeecb591b687411511db86342c97b03d7572b3a4c176'
    ),
    (
        'stack_overflow',
        '899ea08b7de715d80c5fdb4740d2c2cb364094b5',
        'c9aa0629a4a639f706bbd3bcee71743eb56d5d4c1915cb4a7e20e8291696a6fa'
    ),
    (
        'meow',
        '7d5c2a2d6136fbf166211d5183bf66214a247f31',
        '19486a0981d771ebe37dca73340f8b87d65ca06ed125ab78a5332ad077d027e7'
    ),
    (
        'qwerty',
        'b1b3773a05c0ed0176787a4f1574ff0075f7521e',
        '5b34aea025c935b2e32b6dc402deecc7dd27fe88318220a09420208e2367a8b9'
    ),
    (
        'admin',
        'd033e22ae348aeb5660fc2140aec35850c4da997',
        '15756148b4f8c72c1c22feaf23168495970cbe3a26ef957636ea6e4c23097e96'
    );

insert into nthreads values (3);
insert into max_minutes_to_deliver values (30);

update plate_available set many=3, price=10.5 where plate=1;

commit;
