begin;

create or replace language plpythonu;

create or replace function
    update_plate_available() returns trigger as $$
        id = TD['new']['id']
        plan = plpy.prepare('insert into plate_available values($1, 0, null)', ['integer'])
        plpy.execute(plan, [id])
$$ language plpythonu;

create or replace function
    refresh_thread() returns trigger as $$
        t_id = TD['old']['thread']
        nthreads = plpy.execute('select * from nthreads')[0]['nthreads']
        if t_id >= nthreads: return None
        plpy.execute('refresh materialized view thread{}'.format(t_id))
$$ language plpythonu;

create or replace function
    plate_available() returns trigger as $$
        id = TD['new']['plate']
        plan = plpy.prepare('select * from plate_available where plate=$1', ['integer'])
        rv = plpy.execute(plan, [id])
        if len(rv) == 0: raise plpy.Error('Tried to insert a NOT available plate')
$$ language plpythonu;

create or replace function
    nthreads_must_be_unique() returns trigger as $$
        rv = plpy.execute('select * from nthreads')
        if len(rv) > 0: raise plpy.Error('nthreads is always a single value')
$$ language plpythonu;

create or replace function
    nthreads_is_positive() returns trigger as $$
        rv = plpy.execute('select * from nthreads')
        if len(rv) < 1: raise plpy.Error('No thread available')

        nthreads = rv[0]['nthreads']
        if nthreads < 1: raise plpy.Error('No thread available')
$$ language plpythonu;

create type order_t as (
    id text,
    _user text,
    last_name text,
    phone_number text,
    ssn text,
    vat text,
    requested_at timestamp with time zone,
    address_to_deliver text,
    plates integer[]
);

create or replace function
    insert_order(o order_t) returns text as $$
        with plpy.subtransaction():
            plan = plpy.prepare('''
                insert into _order values
                    ($1, $2, $3, $4, $5, $6, $7, $8, null)
            ''', ['text', 'text', 'text', 'text', 'text', 'text', 'text', 'timestamp with time zone'])
            plpy.execute(plan, [
                o['id'], o['_user'], o['last_name'],
                o['phone_number'], o['ssn'], o['vat'],
                o['address_to_deliver'], o['requested_at']
            ])

            for p_id in o['plates']:
                plan = plpy.prepare('''
                    insert into plate_ordered(plate, _order) values ($1, $2)
                ''', ['integer', 'text'])
                plpy.execute(plan, [p_id, o['id']])

            plpy.execute(plpy.prepare('select * from enqueue_plates($1)', ['text']), [o['id']])

            return o['id']
$$ language plpythonu;

create or replace function
    update_availability() returns trigger as $$
        inserting = False
        if 'new' in TD and TD['new']:
            inserting = True

        if inserting:
            id = TD['new']['plate']
        else:
            id = TD['old']['plate']

        plan = plpy.prepare('select many from plate_available where plate=$1', ['integer'])
        rv = plpy.execute(plan, [id])
       
        if len(rv) < 1: return None

        many = rv[0]['many']

        plan = plpy.prepare('update plate_available set many=$1 where plate=$2', ['integer', 'integer'])

        if inserting:
            plpy.execute(plan, [many-1, id])
        else:
            plpy.execute(plan, [many+1, id])
$$ language plpythonu;

create type stat_t as (
    id integer,
    nplates_prepared integer,
    working_minutes float,
    user_served integer,
    average_value_of_plates float
);

create or replace function
    compute_stats_for_thread(id integer) returns stat_t as $$
        nthreads = plpy.execute('select * from nthreads')[0]['nthreads']

        if id >= nthreads: raise plpy.Error('Invalid thread id')

        plpy.execute('refresh materialized view thread{}'.format(id))

        from dateutil.parser import parse
        import datetime

        ret = {}

        ret['id'] = id

        query = 'select count(*) from plate_ordered where thread=$1'
        ret['nplates_prepared'] = \
           plpy.execute(plpy.prepare(query, ['integer']), [id])[0]['count']

        query = 'select * from plate_ordered where thread=$1'        
        rv = plpy.execute(plpy.prepare(query, ['integer']), [id])

        to_datetime = lambda x: parse(x)

        working_minutes = datetime.timedelta(0)
        for r in rv:
            working_minutes += \
               to_datetime(r['proposed_at']) - to_datetime(r['started_at'])
        ret['working_minutes'] = working_minutes.seconds / 60.

        query = '''
            select count(distinct _user)
            from plate_ordered as po, _order as o
            where po._order=o.id and thread={}
        '''.format(id)
        ret['user_served'] = plpy.execute(query)[0]['count']

        query = '''
            select avg(price)
            from plate_ordered as po, plate_available as at
            where po.plate=at.plate and po.thread={}
        '''.format(id)
        ret['average_value_of_plates'] = plpy.execute(query)[0]['avg'] or 0.

        return ret
$$ language plpythonu;

create or replace function
    update_threads() returns trigger as $$
        if 'old' in TD and TD['old']:
            old_nthreads = TD['old']['nthreads']
        else:
            old_nthreads = 0

        if 'new' in TD and TD['new']:
            new_nthreads = TD['new']['nthreads']
        else:
            new_nthreads = 0


        if old_nthreads < new_nthreads:
            for i in range(old_nthreads, new_nthreads):
                plpy.execute('''
                    create materialized view thread{0}
                    as select plate, _order, proposed_at, started_at
                    from plate_ordered where thread={0}
                '''.format(i))

        elif old_nthreads > new_nthreads:
            for i in range(new_nthreads, old_nthreads):
                plpy.execute('''
                    drop materialized view thread{}
                '''.format(i))

$$ language plpythonu;

create or replace function
    enqueue_plates(order_id text) returns timestamp with time zone as $$
        from dateutil.parser import parse
        import datetime

        # What the heck, python?!
        class UTC(datetime.tzinfo):
            def utcoffset(self, dt):
                return datetime.timedelta(0)
            def tzname(self, dt):
                return 'UTC'
            def dst(self, dt):
                return datetime.timedelta(0)
        
        def fetch_nthreads():
            query = 'select * from nthreads'
            return plpy.execute(query)[0]['nthreads']

        def fetch_plate_ids_by_order(id):
            query = '''
                select * 
                from plate_ordered as po, _order as o
                where po._order=o.id and o.id=$1
            '''
            plan = plpy.prepare(query, ['text'])
            return map(lambda x: x['plate'], plpy.execute(plan, [id]))

        def fetch_minutes_needed_for(p_id):
            query = 'select * from plate where id=$1'
            res = plpy.execute(plpy.prepare(query, ['integer']), [p_id])
            return datetime.timedelta(0, int(res[0]['minutes_needed']) * 60)

        def fetch_last_proposed_time_by(t_id):
            fmt = 'select * from thread{} order by proposed_at desc'
            query = fmt.format(t_id)

            res = plpy.execute(query)
            res = filter( \
                lambda r: parse(r['proposed_at']) > datetime.datetime.now(tz=UTC()), \
                res \
            )

            if len(res) == 0:
                return datetime.datetime.now(tz=UTC())
           
            return \
                parse(res[0]['proposed_at'])

        def fetch_best_thread_id_for(p_id):
            lst = []
            min_needed = fetch_minutes_needed_for(p_id)
            n_threads = fetch_nthreads()

            for t_id in range(n_threads):
                tstamp = fetch_last_proposed_time_by(t_id)
                lst.append((t_id, tstamp + min_needed))

            lst.sort(key=lambda x: x[1])
            return lst[0]

        def enqueue(t_id, p_id, proposed_at):
            # Take a deep breath!, and ...
            plpy.execute(plpy.prepare(
                'update plate_ordered set thread=$1 where _order=$2 and plate=$3',
                ['integer', 'text', 'integer']
            ), [t_id, order_id, p_id])

            plpy.execute(plpy.prepare(
                'update plate_ordered set proposed_at=$1 where _order=$2 and plate=$3',
                ['timestamp with time zone', 'text', 'integer']
            ), [proposed_at.isoformat(), order_id, p_id])

            rv = plpy.execute('''
                select * from thread{}
                order by proposed_at desc
            '''.format(t_id))
            rv = filter(lambda r: \
                           parse(r['proposed_at']) > datetime.datetime.now(tz=UTC()), rv)

            if len(rv) > 0:
                started_at = rv[0]['proposed_at']
            else:
                started_at = datetime.datetime.now(tz=UTC()).isoformat()

            plpy.execute(plpy.prepare(
                'update plate_ordered set started_at=$1 where _order=$2 and plate=$3',
                ['timestamp with time zone', 'text', 'integer']
            ), [started_at, order_id, p_id])

            fmt = 'refresh materialized view thread{}'
            plpy.execute(fmt.format(t_id))

        lst = []

        plate_ids = fetch_plate_ids_by_order(order_id)

        if fetch_nthreads() < 1: raise plpy.Error('No thread available')
        if plate_ids < 1: return None

        for p_id in plate_ids:
            t_id, proposed_at = fetch_best_thread_id_for(p_id)

            enqueue(t_id, p_id, proposed_at)

            tstamp = fetch_last_proposed_time_by(t_id)
            lst.append(tstamp)

        ret = sorted(lst)[-1]

        ## Fetch time needed to deliver ...
        destination = plpy.execute( \
                         plpy.prepare('select address_to_deliver from _order where id=$1', ['text']),
                         [order_id]
                      )[0]['address_to_deliver']
        import requests, json

        url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
        url += '?key={}'.format('AIzaSyA3CC9oZCepcSTpq-ens5zTr050VWLAfRU')
        url += '&origins={}'.format('+'.join('Via Comelico, Milano'.split(' ')))
        url += '&destinations={}'.format('+'.join(destination.split(' ')))

        r = requests.get(url)

        if r.status_code != 200:
            raise plpy.Error('Something went wrong with Google Maps API')

        body = json.loads(r.text)

        if body['status'] != 'OK':
            raise plpy.Error('Something went wrong with Google Maps API')

        seconds_needed = body['rows'][0]['elements'][0]['duration']['value']
        try:
            max_seconds_to_deliver = \
               plpy.execute('''
                   select * from max_minutes_to_deliver
               ''')[0]['max_minutes_to_deliver'] * 60
            max_seconds_to_deliver = datetime.timedelta(0, max_seconds_to_deliver)
        except Exception:
            max_seconds_to_deliver = None

        seconds_needed = datetime.timedelta(0, seconds_needed)

        if max_seconds_to_deliver and seconds_needed > max_seconds_to_deliver:
            raise plpy.Error('Order cannot be delivered due to admin constraint')

        ret += seconds_needed

        plpy.execute(plpy.prepare(
            'update _order set proposed_at=$1 where id=$2',
            ['timestamp with time zone', 'text']
        ), [ret.isoformat(), order_id])

        return ret.isoformat()
$$ language plpythonu;

commit;

-- vim: set filetype=python:
