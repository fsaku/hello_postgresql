begin;

create table plate (
    id serial primary key,
    name text not null,
    img_url text,
    description text,
    minutes_needed integer not null check (minutes_needed >= 0),
    complexity integer check (1 <= complexity and complexity <= 5)
);

create table plate_available (
    plate integer references plate(id) on delete cascade,
    many integer check (many >= 0),
    price float check (price > 0.)
);

create table _user (
    id text primary key
);

create table _order (
    id text primary key,
    _user text references _user(id),
    last_name text not null,
    phone_number text,
    ssn text,
    vat text,
    address_to_deliver text not null,
    requested_at timestamp with time zone,
    proposed_at timestamp with time zone
);

create table plate_ordered (
    plate integer references plate(id),
    _order text references _order(id) on delete cascade,
    proposed_at timestamp with time zone,
    started_at timestamp with time zone,
    thread integer
);

create table nthreads (
    nthreads integer check (nthreads >= 0)
);

create table max_minutes_to_deliver (
    max_minutes_to_deliver integer check (max_minutes_to_deliver >= 0)
);

create table auth_data (
    _user text references _user(id) on delete cascade,
    sha1sum text,
    auth_token text
);

create index on auth_data(auth_token);

commit;
