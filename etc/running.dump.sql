--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: plpythonu; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: crusty
--

CREATE OR REPLACE PROCEDURAL LANGUAGE plpythonu;


ALTER PROCEDURAL LANGUAGE plpythonu OWNER TO crusty;

SET search_path = public, pg_catalog;

--
-- Name: order_t; Type: TYPE; Schema: public; Owner: crusty
--

CREATE TYPE order_t AS (
	id text,
	_user text,
	last_name text,
	phone_number text,
	ssn text,
	vat text,
	requested_at timestamp with time zone,
	address_to_deliver text,
	plates integer[]
);


ALTER TYPE order_t OWNER TO crusty;

--
-- Name: stat_t; Type: TYPE; Schema: public; Owner: crusty
--

CREATE TYPE stat_t AS (
	id integer,
	nplates_prepared integer,
	working_minutes double precision,
	user_served integer,
	average_value_of_plates double precision
);


ALTER TYPE stat_t OWNER TO crusty;

--
-- Name: compute_stats_for_thread(integer); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION compute_stats_for_thread(id integer) RETURNS stat_t
    LANGUAGE plpythonu
    AS $_$
        nthreads = plpy.execute('select * from nthreads')[0]['nthreads']

        if id >= nthreads: raise plpy.Error('Invalid thread id')

        plpy.execute('refresh materialized view thread{}'.format(id))

        from dateutil.parser import parse
        import datetime

        ret = {}

        ret['id'] = id

        query = 'select count(*) from plate_ordered where thread=$1'
        ret['nplates_prepared'] = \
           plpy.execute(plpy.prepare(query, ['integer']), [id])[0]['count']

        query = 'select * from plate_ordered where thread=$1'        
        rv = plpy.execute(plpy.prepare(query, ['integer']), [id])

        to_datetime = lambda x: parse(x)

        working_minutes = datetime.timedelta(0)
        for r in rv:
            working_minutes += \
               to_datetime(r['proposed_at']) - to_datetime(r['started_at'])
        ret['working_minutes'] = working_minutes.seconds / 60.

        query = '''
            select count(distinct _user)
            from plate_ordered as po, _order as o
            where po._order=o.id and thread={}
        '''.format(id)
        ret['user_served'] = plpy.execute(query)[0]['count']

        query = '''
            select avg(price)
            from plate_ordered as po, plate_available as at
            where po.plate=at.plate and po.thread={}
        '''.format(id)
        ret['average_value_of_plates'] = plpy.execute(query)[0]['avg'] or 0.

        return ret
$_$;


ALTER FUNCTION public.compute_stats_for_thread(id integer) OWNER TO crusty;

--
-- Name: enqueue_plates(text); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION enqueue_plates(order_id text) RETURNS timestamp with time zone
    LANGUAGE plpythonu
    AS $_$
        from dateutil.parser import parse
        import datetime

        # What the heck, python?!
        class UTC(datetime.tzinfo):
            def utcoffset(self, dt):
                return datetime.timedelta(0)
            def tzname(self, dt):
                return 'UTC'
            def dst(self, dt):
                return datetime.timedelta(0)
        
        def fetch_nthreads():
            query = 'select * from nthreads'
            return plpy.execute(query)[0]['nthreads']

        def fetch_plate_ids_by_order(id):
            query = '''
                select * 
                from plate_ordered as po, _order as o
                where po._order=o.id and o.id=$1
            '''
            plan = plpy.prepare(query, ['text'])
            return map(lambda x: x['plate'], plpy.execute(plan, [id]))

        def fetch_minutes_needed_for(p_id):
            query = 'select * from plate where id=$1'
            res = plpy.execute(plpy.prepare(query, ['integer']), [p_id])
            return datetime.timedelta(0, int(res[0]['minutes_needed']) * 60)

        def fetch_last_proposed_time_by(t_id):
            fmt = 'select * from thread{} order by proposed_at desc'
            query = fmt.format(t_id)

            res = plpy.execute(query)
            res = filter( \
                lambda r: parse(r['proposed_at']) > datetime.datetime.now(tz=UTC()), \
                res \
            )

            if len(res) == 0:
                return datetime.datetime.now(tz=UTC())
           
            return \
                parse(res[0]['proposed_at'])

        def fetch_best_thread_id_for(p_id):
            lst = []
            min_needed = fetch_minutes_needed_for(p_id)
            n_threads = fetch_nthreads()

            for t_id in range(n_threads):
                tstamp = fetch_last_proposed_time_by(t_id)
                lst.append((t_id, tstamp + min_needed))

            lst.sort(key=lambda x: x[1])
            return lst[0]

        def enqueue(t_id, p_id, proposed_at):
            # Take a deep breath!, and ...
            plpy.execute(plpy.prepare(
                'update plate_ordered set thread=$1 where _order=$2 and plate=$3',
                ['integer', 'text', 'integer']
            ), [t_id, order_id, p_id])

            plpy.execute(plpy.prepare(
                'update plate_ordered set proposed_at=$1 where _order=$2 and plate=$3',
                ['timestamp with time zone', 'text', 'integer']
            ), [proposed_at.isoformat(), order_id, p_id])

            rv = plpy.execute('''
                select * from thread{}
                order by proposed_at desc
            '''.format(t_id))
            rv = filter(lambda r: \
                           parse(r['proposed_at']) > datetime.datetime.now(tz=UTC()), rv)

            if len(rv) > 0:
                started_at = rv[0]['proposed_at']
            else:
                started_at = datetime.datetime.now(tz=UTC()).isoformat()

            plpy.execute(plpy.prepare(
                'update plate_ordered set started_at=$1 where _order=$2 and plate=$3',
                ['timestamp with time zone', 'text', 'integer']
            ), [started_at, order_id, p_id])

            fmt = 'refresh materialized view thread{}'
            plpy.execute(fmt.format(t_id))

        lst = []

        plate_ids = fetch_plate_ids_by_order(order_id)

        if fetch_nthreads() < 1: raise plpy.Error('No thread available')
        if plate_ids < 1: return None

        for p_id in plate_ids:
            t_id, proposed_at = fetch_best_thread_id_for(p_id)

            enqueue(t_id, p_id, proposed_at)

            tstamp = fetch_last_proposed_time_by(t_id)
            lst.append(tstamp)

        ret = sorted(lst)[-1]

        ## Fetch time needed to deliver ...
        destination = plpy.execute( \
                         plpy.prepare('select address_to_deliver from _order where id=$1', ['text']),
                         [order_id]
                      )[0]['address_to_deliver']
        import requests, json

        url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
        url += '?key={}'.format('AIzaSyA3CC9oZCepcSTpq-ens5zTr050VWLAfRU')
        url += '&origins={}'.format('+'.join('Via Comelico, Milano'.split(' ')))
        url += '&destinations={}'.format('+'.join(destination.split(' ')))

        r = requests.get(url)

        if r.status_code != 200:
            raise plpy.Error('Something went wrong with Google Maps API')

        body = json.loads(r.text)

        if body['status'] != 'OK':
            raise plpy.Error('Something went wrong with Google Maps API')

        seconds_needed = body['rows'][0]['elements'][0]['duration']['value']
        try:
            max_seconds_to_deliver = \
               plpy.execute('''
                   select * from max_minutes_to_deliver
               ''')[0]['max_minutes_to_deliver'] * 60
            max_seconds_to_deliver = datetime.timedelta(0, max_seconds_to_deliver)
        except Exception:
            max_seconds_to_deliver = None

        seconds_needed = datetime.timedelta(0, seconds_needed)

        if max_seconds_to_deliver and seconds_needed > max_seconds_to_deliver:
            raise plpy.Error('Order cannot be delivered due to admin constraint')

        ret += seconds_needed

        plpy.execute(plpy.prepare(
            'update _order set proposed_at=$1 where id=$2',
            ['timestamp with time zone', 'text']
        ), [ret.isoformat(), order_id])

        return ret.isoformat()
$_$;


ALTER FUNCTION public.enqueue_plates(order_id text) OWNER TO crusty;

--
-- Name: insert_order(order_t); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION insert_order(o order_t) RETURNS text
    LANGUAGE plpythonu
    AS $_$
        with plpy.subtransaction():
            plan = plpy.prepare('''
                insert into _order values
                    ($1, $2, $3, $4, $5, $6, $7, $8, null)
            ''', ['text', 'text', 'text', 'text', 'text', 'text', 'text', 'timestamp with time zone'])
            plpy.execute(plan, [
                o['id'], o['_user'], o['last_name'],
                o['phone_number'], o['ssn'], o['vat'],
                o['address_to_deliver'], o['requested_at']
            ])

            for p_id in o['plates']:
                plan = plpy.prepare('''
                    insert into plate_ordered(plate, _order) values ($1, $2)
                ''', ['integer', 'text'])
                plpy.execute(plan, [p_id, o['id']])

            plpy.execute(plpy.prepare('select * from enqueue_plates($1)', ['text']), [o['id']])

            return o['id']
$_$;


ALTER FUNCTION public.insert_order(o order_t) OWNER TO crusty;

--
-- Name: nthreads_is_positive(); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION nthreads_is_positive() RETURNS trigger
    LANGUAGE plpythonu
    AS $$
        rv = plpy.execute('select * from nthreads')
        if len(rv) < 1: raise plpy.Error('No thread available')

        nthreads = rv[0]['nthreads']
        if nthreads < 1: raise plpy.Error('No thread available')
$$;


ALTER FUNCTION public.nthreads_is_positive() OWNER TO crusty;

--
-- Name: nthreads_must_be_unique(); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION nthreads_must_be_unique() RETURNS trigger
    LANGUAGE plpythonu
    AS $$
        rv = plpy.execute('select * from nthreads')
        if len(rv) > 0: raise plpy.Error('nthreads is always a single value')
$$;


ALTER FUNCTION public.nthreads_must_be_unique() OWNER TO crusty;

--
-- Name: plate_available(); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION plate_available() RETURNS trigger
    LANGUAGE plpythonu
    AS $_$
        id = TD['new']['plate']
        plan = plpy.prepare('select * from plate_available where plate=$1', ['integer'])
        rv = plpy.execute(plan, [id])
        if len(rv) == 0: raise plpy.Error('Tried to insert a NOT available plate')
$_$;


ALTER FUNCTION public.plate_available() OWNER TO crusty;

--
-- Name: refresh_thread(); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION refresh_thread() RETURNS trigger
    LANGUAGE plpythonu
    AS $$
        t_id = TD['old']['thread']
        nthreads = plpy.execute('select * from nthreads')[0]['nthreads']
        if t_id >= nthreads: return None
        plpy.execute('refresh materialized view thread{}'.format(t_id))
$$;


ALTER FUNCTION public.refresh_thread() OWNER TO crusty;

--
-- Name: update_availability(); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION update_availability() RETURNS trigger
    LANGUAGE plpythonu
    AS $_$
        inserting = False
        if 'new' in TD and TD['new']:
            inserting = True

        if inserting:
            id = TD['new']['plate']
        else:
            id = TD['old']['plate']

        plan = plpy.prepare('select many from plate_available where plate=$1', ['integer'])
        rv = plpy.execute(plan, [id])
       
        if len(rv) < 1: return None

        many = rv[0]['many']

        plan = plpy.prepare('update plate_available set many=$1 where plate=$2', ['integer', 'integer'])

        if inserting:
            plpy.execute(plan, [many-1, id])
        else:
            plpy.execute(plan, [many+1, id])
$_$;


ALTER FUNCTION public.update_availability() OWNER TO crusty;

--
-- Name: update_plate_available(); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION update_plate_available() RETURNS trigger
    LANGUAGE plpythonu
    AS $_$
        id = TD['new']['id']
        plan = plpy.prepare('insert into plate_available values($1, 0, null)', ['integer'])
        plpy.execute(plan, [id])
$_$;


ALTER FUNCTION public.update_plate_available() OWNER TO crusty;

--
-- Name: update_threads(); Type: FUNCTION; Schema: public; Owner: crusty
--

CREATE FUNCTION update_threads() RETURNS trigger
    LANGUAGE plpythonu
    AS $$
        if 'old' in TD and TD['old']:
            old_nthreads = TD['old']['nthreads']
        else:
            old_nthreads = 0

        if 'new' in TD and TD['new']:
            new_nthreads = TD['new']['nthreads']
        else:
            new_nthreads = 0


        if old_nthreads < new_nthreads:
            for i in range(old_nthreads, new_nthreads):
                plpy.execute('''
                    create materialized view thread{0}
                    as select plate, _order, proposed_at, started_at
                    from plate_ordered where thread={0}
                '''.format(i))

        elif old_nthreads > new_nthreads:
            for i in range(new_nthreads, old_nthreads):
                plpy.execute('''
                    drop materialized view thread{}
                '''.format(i))

$$;


ALTER FUNCTION public.update_threads() OWNER TO crusty;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: _order; Type: TABLE; Schema: public; Owner: crusty
--

CREATE TABLE _order (
    id text NOT NULL,
    _user text,
    last_name text NOT NULL,
    phone_number text,
    ssn text,
    vat text,
    address_to_deliver text NOT NULL,
    requested_at timestamp with time zone,
    proposed_at timestamp with time zone
);


ALTER TABLE _order OWNER TO crusty;

--
-- Name: _user; Type: TABLE; Schema: public; Owner: crusty
--

CREATE TABLE _user (
    id text NOT NULL
);


ALTER TABLE _user OWNER TO crusty;

--
-- Name: auth_data; Type: TABLE; Schema: public; Owner: crusty
--

CREATE TABLE auth_data (
    _user text,
    sha1sum text,
    auth_token text
);


ALTER TABLE auth_data OWNER TO crusty;

--
-- Name: max_minutes_to_deliver; Type: TABLE; Schema: public; Owner: crusty
--

CREATE TABLE max_minutes_to_deliver (
    max_minutes_to_deliver integer,
    CONSTRAINT max_minutes_to_deliver_max_minutes_to_deliver_check CHECK ((max_minutes_to_deliver >= 0))
);


ALTER TABLE max_minutes_to_deliver OWNER TO crusty;

--
-- Name: nthreads; Type: TABLE; Schema: public; Owner: crusty
--

CREATE TABLE nthreads (
    nthreads integer,
    CONSTRAINT nthreads_nthreads_check CHECK ((nthreads >= 0))
);


ALTER TABLE nthreads OWNER TO crusty;

--
-- Name: plate; Type: TABLE; Schema: public; Owner: crusty
--

CREATE TABLE plate (
    id integer NOT NULL,
    name text NOT NULL,
    img_url text,
    description text,
    minutes_needed integer NOT NULL,
    complexity integer,
    CONSTRAINT plate_complexity_check CHECK (((1 <= complexity) AND (complexity <= 5))),
    CONSTRAINT plate_minutes_needed_check CHECK ((minutes_needed >= 0))
);


ALTER TABLE plate OWNER TO crusty;

--
-- Name: plate_available; Type: TABLE; Schema: public; Owner: crusty
--

CREATE TABLE plate_available (
    plate integer,
    many integer,
    price double precision,
    CONSTRAINT plate_available_many_check CHECK ((many >= 0)),
    CONSTRAINT plate_available_price_check CHECK ((price > ('0'::numeric)::double precision))
);


ALTER TABLE plate_available OWNER TO crusty;

--
-- Name: plate_id_seq; Type: SEQUENCE; Schema: public; Owner: crusty
--

CREATE SEQUENCE plate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plate_id_seq OWNER TO crusty;

--
-- Name: plate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crusty
--

ALTER SEQUENCE plate_id_seq OWNED BY plate.id;


--
-- Name: plate_ordered; Type: TABLE; Schema: public; Owner: crusty
--

CREATE TABLE plate_ordered (
    plate integer,
    _order text,
    proposed_at timestamp with time zone,
    started_at timestamp with time zone,
    thread integer
);


ALTER TABLE plate_ordered OWNER TO crusty;

--
-- Name: thread0; Type: MATERIALIZED VIEW; Schema: public; Owner: crusty
--

CREATE MATERIALIZED VIEW thread0 AS
 SELECT plate_ordered.plate,
    plate_ordered._order,
    plate_ordered.proposed_at,
    plate_ordered.started_at
   FROM plate_ordered
  WHERE (plate_ordered.thread = 0)
  WITH NO DATA;


ALTER TABLE thread0 OWNER TO crusty;

--
-- Name: thread1; Type: MATERIALIZED VIEW; Schema: public; Owner: crusty
--

CREATE MATERIALIZED VIEW thread1 AS
 SELECT plate_ordered.plate,
    plate_ordered._order,
    plate_ordered.proposed_at,
    plate_ordered.started_at
   FROM plate_ordered
  WHERE (plate_ordered.thread = 1)
  WITH NO DATA;


ALTER TABLE thread1 OWNER TO crusty;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY plate ALTER COLUMN id SET DEFAULT nextval('plate_id_seq'::regclass);


--
-- Data for Name: _order; Type: TABLE DATA; Schema: public; Owner: crusty
--

COPY _order (id, _user, last_name, phone_number, ssn, vat, address_to_deliver, requested_at, proposed_at) FROM stdin;
f178504e5df06e4d	mean	Lanzetti				Viale Umbria, Milano	\N	2016-04-03 16:48:41.056615+02
94b9c0e66e1e7019	stack_overflow	Vincenzetti				Via Comelico, Milano	2016-04-03 14:30:00+02	2016-04-03 17:16:13.07673+02
44ccad85f0aa1c35	mean	Lanzetti				Viale Umbria, Milano	\N	2016-04-03 17:18:41.056615+02
\.


--
-- Data for Name: _user; Type: TABLE DATA; Schema: public; Owner: crusty
--

COPY _user (id) FROM stdin;
mean
meow
qwerty
stack_overflow
admin
\.


--
-- Data for Name: auth_data; Type: TABLE DATA; Schema: public; Owner: crusty
--

COPY auth_data (_user, sha1sum, auth_token) FROM stdin;
mean	5f00f7a0a9f5e57deb39805aa521121fa277e75e	d55564d563206dad63f4eeecb591b687411511db86342c97b03d7572b3a4c176
meow	7d5c2a2d6136fbf166211d5183bf66214a247f31	19486a0981d771ebe37dca73340f8b87d65ca06ed125ab78a5332ad077d027e7
qwerty	b1b3773a05c0ed0176787a4f1574ff0075f7521e	5b34aea025c935b2e32b6dc402deecc7dd27fe88318220a09420208e2367a8b9
admin	d033e22ae348aeb5660fc2140aec35850c4da997	15756148b4f8c72c1c22feaf23168495970cbe3a26ef957636ea6e4c23097e96
stack_overflow	899ea08b7de715d80c5fdb4740d2c2cb364094b5	c9aa0629a4a639f706bbd3bcee71743eb56d5d4c1915cb4a7e20e8291696a6fa
\.


--
-- Data for Name: max_minutes_to_deliver; Type: TABLE DATA; Schema: public; Owner: crusty
--

COPY max_minutes_to_deliver (max_minutes_to_deliver) FROM stdin;
120
\.


--
-- Data for Name: nthreads; Type: TABLE DATA; Schema: public; Owner: crusty
--

COPY nthreads (nthreads) FROM stdin;
2
\.


--
-- Data for Name: plate; Type: TABLE DATA; Schema: public; Owner: crusty
--

COPY plate (id, name, img_url, description, minutes_needed, complexity) FROM stdin;
1	Lasagne alla Morricone	\N	\N	45	\N
2	Puntarelle alla romana	\N	\N	10	\N
3	Spaghetti di Mario Brega	\N	\N	20	\N
4	Caprese con pasta d'acciughe	\N	\N	5	\N
5	Ossobuco al pesto	\N	\N	50	\N
6	Pollo mele, limone e finocchi	\N	\N	10	\N
7	Penne all'arrabbiata	\N	\N	30	\N
\.


--
-- Data for Name: plate_available; Type: TABLE DATA; Schema: public; Owner: crusty
--

COPY plate_available (plate, many, price) FROM stdin;
2	0	\N
3	0	\N
4	0	\N
6	0	\N
1	3	10.5
5	8	10.5
7	18	10.8000000000000007
\.


--
-- Name: plate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crusty
--

SELECT pg_catalog.setval('plate_id_seq', 7, true);


--
-- Data for Name: plate_ordered; Type: TABLE DATA; Schema: public; Owner: crusty
--

COPY plate_ordered (plate, _order, proposed_at, started_at, thread) FROM stdin;
5	f178504e5df06e4d	2016-04-03 16:46:13.056615+02	2016-04-03 15:56:13.058804+02	0
7	f178504e5df06e4d	2016-04-03 16:26:13.07673+02	2016-04-03 15:56:13.07821+02	1
5	94b9c0e66e1e7019	2016-04-03 17:16:13.07673+02	2016-04-03 16:26:13.07673+02	1
7	44ccad85f0aa1c35	2016-04-03 17:16:13.056615+02	2016-04-03 16:46:13.056615+02	0
\.


--
-- Name: _order_pkey; Type: CONSTRAINT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY _order
    ADD CONSTRAINT _order_pkey PRIMARY KEY (id);


--
-- Name: _user_pkey; Type: CONSTRAINT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY _user
    ADD CONSTRAINT _user_pkey PRIMARY KEY (id);


--
-- Name: plate_pkey; Type: CONSTRAINT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY plate
    ADD CONSTRAINT plate_pkey PRIMARY KEY (id);


--
-- Name: auth_data_auth_token_idx; Type: INDEX; Schema: public; Owner: crusty
--

CREATE INDEX auth_data_auth_token_idx ON auth_data USING btree (auth_token);


--
-- Name: nthreads_is_positive; Type: TRIGGER; Schema: public; Owner: crusty
--

CREATE TRIGGER nthreads_is_positive BEFORE INSERT ON _order FOR EACH ROW EXECUTE PROCEDURE nthreads_is_positive();


--
-- Name: nthreads_must_be_unique; Type: TRIGGER; Schema: public; Owner: crusty
--

CREATE TRIGGER nthreads_must_be_unique BEFORE INSERT ON nthreads FOR EACH STATEMENT EXECUTE PROCEDURE nthreads_must_be_unique();


--
-- Name: plate_available; Type: TRIGGER; Schema: public; Owner: crusty
--

CREATE TRIGGER plate_available BEFORE INSERT ON plate_ordered FOR EACH ROW EXECUTE PROCEDURE plate_available();


--
-- Name: refresh_thread; Type: TRIGGER; Schema: public; Owner: crusty
--

CREATE TRIGGER refresh_thread AFTER DELETE ON plate_ordered FOR EACH ROW EXECUTE PROCEDURE refresh_thread();


--
-- Name: update_availability; Type: TRIGGER; Schema: public; Owner: crusty
--

CREATE TRIGGER update_availability AFTER INSERT OR DELETE ON plate_ordered FOR EACH ROW EXECUTE PROCEDURE update_availability();


--
-- Name: update_plate_available; Type: TRIGGER; Schema: public; Owner: crusty
--

CREATE TRIGGER update_plate_available AFTER INSERT ON plate FOR EACH ROW EXECUTE PROCEDURE update_plate_available();


--
-- Name: update_threads; Type: TRIGGER; Schema: public; Owner: crusty
--

CREATE TRIGGER update_threads AFTER INSERT OR DELETE OR UPDATE ON nthreads FOR EACH ROW EXECUTE PROCEDURE update_threads();


--
-- Name: _order__user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY _order
    ADD CONSTRAINT _order__user_fkey FOREIGN KEY (_user) REFERENCES _user(id);


--
-- Name: auth_data__user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY auth_data
    ADD CONSTRAINT auth_data__user_fkey FOREIGN KEY (_user) REFERENCES _user(id) ON DELETE CASCADE;


--
-- Name: plate_available_plate_fkey; Type: FK CONSTRAINT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY plate_available
    ADD CONSTRAINT plate_available_plate_fkey FOREIGN KEY (plate) REFERENCES plate(id) ON DELETE CASCADE;


--
-- Name: plate_ordered__order_fkey; Type: FK CONSTRAINT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY plate_ordered
    ADD CONSTRAINT plate_ordered__order_fkey FOREIGN KEY (_order) REFERENCES _order(id) ON DELETE CASCADE;


--
-- Name: plate_ordered_plate_fkey; Type: FK CONSTRAINT; Schema: public; Owner: crusty
--

ALTER TABLE ONLY plate_ordered
    ADD CONSTRAINT plate_ordered_plate_fkey FOREIGN KEY (plate) REFERENCES plate(id);


--
-- Name: thread0; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: crusty
--

REFRESH MATERIALIZED VIEW thread0;


--
-- Name: thread1; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: crusty
--

REFRESH MATERIALIZED VIEW thread1;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

