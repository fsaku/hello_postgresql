begin;

create trigger nthreads_must_be_unique
    before insert on nthreads 
    execute procedure nthreads_must_be_unique();

create trigger update_threads
    after update or insert or delete on nthreads 
    for each row execute procedure update_threads();

create trigger refresh_thread
    after delete on plate_ordered
    for each row execute procedure refresh_thread();

create trigger plate_available 
    before insert on plate_ordered
    for each row execute procedure plate_available();

create trigger update_availability 
    after insert or delete on plate_ordered
    for each row execute procedure update_availability();

create trigger nthreads_is_positive 
    before insert on _order
    for each row execute procedure nthreads_is_positive();

create trigger update_plate_available
    after insert on plate
    for each row execute procedure update_plate_available();

commit;
