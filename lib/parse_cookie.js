'use strict';

module.exports = parse_cookie;

function
parse_cookie(cookie_str) {
    let ret = {};
    let _lst = cookie_str
                   .trim()
                   .split(';')
                   .map((x) => x.trim())
                   .filter((x) => x !== '')
                   .map((x) => x.split('='))
    for (let kv of _lst) {
        ret[kv[0]] = kv[1];
    }
    return ret;
}
