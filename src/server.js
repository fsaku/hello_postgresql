'use strict';

const pg = require('pg');

const express = require('express');
const server = express();

const root = require('./routers/root');
const api = require('./routers/api');

server.use('/', root);
server.use('/api', api);

module.exports = server;
