'use strict';

function
parseTimeInput(str) {
    var fmt = /([0-9]{1,2})[\.:]([0-9]{1,2})/;

    var hour = zeroLeftPad(str.match(fmt)[1], 2);
    var minutes = zeroLeftPad(str.match(fmt)[2], 2);

    console.log(`hour: ${hour}, minutes: ${minutes}`);

    var year = new Date().getFullYear();
    var month = zeroLeftPad((new Date().getMonth() +1).toString(), 2);
    var day = zeroLeftPad(new Date().getDate().toString(), 2);

    return new Date(
        Date.parse(`${year}-${month}-${day}T${hour}:${minutes}:00+02:00`)
    ).toISOString();
}

function
redirect(where) {
    if (where === '.') {
        return location = location.href;
    } else {
        return location = where;
    }
}

function
toHexString(buf) {
    return Array.prototype.map.call(new Uint8Array(buf), function(b) {
        return zeroLeftPad(b.toString(16), 2);
    }).join('');
}

function
randomOrderID() {
    var v = new Uint8Array(8);
    crypto.getRandomValues(v);
    return toHexString(v);
}

function
parseCookie(str) {
    var ret = {};
    var lst = str.trim().split(';')
    .map(function(x) {
        return x.trim();
    })
    .filter(function(x) {
        return x;
    })
    .map(function(x) {
        return x.split('=');
    });
    lst.forEach(function(kv) {
        var k = kv[0];
        var v = kv[1];
        ret[k] = v;
    });
    return ret;
}

function
zeroLeftPad(str, len) {
    while (str.length < len) {
        str = '0' + str;
    }
    return str;
};

var http = {};
http.request = function(method, url, data, cb) {
    var xhr = new XMLHttpRequest();
    xhr.onprogress = function(e) {
        return console.log(`*** Loaded ${e.loaded} bytes ...`);
    };
    xhr.onreadystatechange = function() {
        if (this.readyState === 4) {
            return cb(null, this.response);
        }
    };
    xhr.onerror = cb;
    xhr.responseType = 'json';
    xhr.open(method, url);
    xhr.withCredentials = true;
    xhr.send(data);
};
http.get = function(url, cb) {
    return http.request('GET', url, null, cb);
};
http.post = function(url, data, cb) {
    return http.request('POST', url, data, cb);
};
http.delete = function(url, cb) {
    return http.request('DELETE', url, null, cb);
}
http.put = function(url, data, cb) {
    return http.request('PUT', url, data, cb);
};


function
fail(err) {
    var alert = document.querySelector('div.alert');
    alert.classList.remove('hidden');
    console.error(err);
}

function
kickout() {
    document.cookie = 'auth_token=';
    return redirect('.');
}
