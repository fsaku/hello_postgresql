'use strict';

const pg = require('pg');
const connection_string = 'postgres://localhost:5432/test';
const parse_cookie = require('../../lib/parse_cookie');

module.exports = {
    as_admin_only: as_admin_only,
    fail: fail,
};

function
as_admin_only(fn) {
    return function(req, res) {
        pg.connect(connection_string, function(err, client, done) {
            if (err) {
                done();
                return fail(new Error(`db.connection, ${err.message}`), res);
            }

            let auth_token = '';
            try {
                auth_token = parse_cookie(req.headers['cookie'])['auth_token'];
            } catch (e) {
                done();
                return fail(new Error('middleware.permission.denied'), res);
            }

            let query = client.query('select * from auth_data where auth_token=$1', [auth_token]);

            query.on('error', function(err) {
                done();
                return fail(new Error(`db.query.failed, ${err.message}`), res);
            });

            let rows = [];

            query.on('row', function(row) {
                rows.push(row);
            });

            query.on('end', function() {
                if (rows.length > 1) {
                    done();
                    return fail(new Error(`db.inconsistency`), res);
                }

                if (rows.length === 1 && rows[0]['_user'] === 'admin') {
                    done();
                    return fn(req, res);
                }

                done();
                return fail(new Error('middleware.permission.denied'), res);
            });
        });
    };
}

function
fail(err, res) {
    let ret = {
        error: err.message,
    };
    res.writeHead(500);
    return res.end(JSON.stringify(ret));
}

