'use strict';

const express = require('express');
const router = express.Router();
const pg = require('pg');
const assert = require('assert');

const parse_cookie = require('../../lib/parse_cookie');
const random_auth_token = require('../../lib/random_auth_token');

const as_admin_only = require('./utility').as_admin_only;
const fail = require('./utility').fail;

const connection_string = 'postgres://localhost:5432/test';

router.use(function(req, res, next) {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', 'http://localhost:8000');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE');
    next();
});

router.get('/meta', function(req, res) {
    pg.connect(connection_string, function(err, client, done) {
        if (err) {
            done();
            return fail(new Error(`db.connection, ${err.message}`), res);
        }

        let query = client.query('select * from nthreads');
        let lst = [];

        query.on('row', function(row) {
            lst.push(row);
        });

        query.on('error', function(err) {
            done();
            return fail(new Error(`db.query.failed, ${err.message}`), res);
        });

        query.on('end', function() {
            if (lst.length > 1) {
                done();
                return fail(new Error('db.consistency'), res);
            }

            let nthreads = 0;
            if (lst.length > 0) {
                nthreads = lst[0]['nthreads'];
            }

            let query = client.query('select * from max_minutes_to_deliver');

            query.on('error', function(err) {
                done();
                return fail(new Error(`db.query.failed, ${err.message}`), res);
            });

            lst = [];

            query.on('row', function(row) {
                lst.push(row);
            });

            query.on('end', function() {
                let max_minutes_to_deliver = null;
                if (lst.length > 0) {
                    max_minutes_to_deliver = lst[0]['max_minutes_to_deliver'];
                }

                done();
                return res.end(JSON.stringify({
                    nthreads: nthreads,
                    max_minutes_to_deliver: max_minutes_to_deliver,
                }));
            });
        });
    });
});

router.post('/meta', as_admin_only(function(req, res) {
    let lst = [];

    req.on('data', function(chunk) {
        lst.push(chunk);
    });

    req.on('end', function() {
        let body = {};

        try {
            body = JSON.parse(lst.join(''));
        } catch (e) {
            return fail(new Error(`middleware.malformed.request, ${e.message}`), res);
        }

        let counter = 0;

        if (body['nthreads']) {
            counter += 1;
        }

        if (body['max_minutes_to_deliver']) {
            counter += 1;
        }

        if (counter === 0) {
            return fail(new Error('middleware.parameter.missing'), res);
        }

        pg.connect(connection_string, function(err, client, done) {
            if (err) {
                done();
                return fail(new Error(`db.connection, ${err.message}`), res);
            }

            if (body['nthreads']) {
                let query = client.query('update nthreads set nthreads=$1', [body['nthreads']]);

                query.on('error', function(err) {
                    done();
                    return fail(new Error(`db.query.failed, ${err.message}`), res);
                });

                query.on('end', function() {
                    counter -= 1;

                    if (counter === 0) {
                        done();
                        return res.end(JSON.stringify({
                            nthreads: body['nthreads'],
                            max_minutes_to_deliver: body['max_minutes_to_deliver'],
                        }));
                    }
                });
            }


            if (body['max_minutes_to_deliver']) {
                let query = client.query('update max_minutes_to_deliver set max_minutes_to_deliver=$1', [body['max_minutes_to_deliver']]);

                query.on('error', function(err) {
                    done();
                    return fail(new Error(`db.query.failed, ${err.message}`), res);
                });

                query.on('end', function() {
                    counter -= 1;

                    if (counter === 0) {
                        done();
                        return res.end(JSON.stringify({
                            nthreads: body['nthreads'],
                            max_minutes_to_deliver: body['max_minutes_to_deliver'],
                        }));
                    }
                });
            }
        });
    });
}));

router.put('/auth', function(req, res) {
    let lst = [];

    req.on('data', function(chunk) {
        lst.push(chunk);
    });

    req.on('end', function() {
        let body = {};

        try {
            body = JSON.parse(lst.join(''));
        } catch (e) {
            return fail(new Error(`middleware.malformed.request, ${e.message}`), res);
        }

        if (!body['sha1sum']) {
            return fail(new Error('middleware.malformed.request'), res);
        }

        let auth_token = null;
        try {
            auth_token = parse_cookie(req.headers.cookie).auth_token;
        } catch (e) {
            return fail(new Error('middleware.permission.denied'), res);
        }

        pg.connect(connection_string, function(err, client, done) {
            if (err) {
                done();
                return fail(new Error(`db.connection, ${err.message}`), res);
            }

            let query = client.query('update auth_data set sha1sum=$1 where auth_token=$2', [body['sha1sum'], auth_token]);

            query.on('error', function(err) {
                done();
                return fail(new Error(err), res);
            });

            query.on('end', function() {
                done();
                return res.end(JSON.stringify({
                    error: null,
                }));
            });
        });
    });
});

router.post('/auth', function(req, res) {
    let lst = [];

    req.on('data', function(chunk) {
        lst.push(chunk);
    });

    req.on('end', function() {
        let body = {};

        try {
            body = JSON.parse(lst.join(''));
        } catch (e) {
            return fail(new Error(`middleware.malformed.request, ${e.message}`), res);
        }

        if (!body['user'] || !body['sha1sum']) {
            return fail(new Error('middleware.malformed.request'), res);
        }

        pg.connect(connection_string, function(err, client, done) {
            if (err) {
                done();
                return fail(new Error(`db.connection, ${err.message}`), res);
            }

            let query = client.query('select * from auth_data where _user=$1', [body['user']]);

            query.on('error', function(err) {
                done();
                return fail(new Error(`db.query.failed, ${err.message}`), res);
            });

            let rows = [];

            query.on('row', function(row) {
                rows.push(row);
            });

            query.on('end', function() {
                if (rows.length > 1) {
                    done();
                    return fail(new Error('db.consistency'), res);
                }

                if (rows.length === 0) {
                    let auth_token = random_auth_token();

                    let user = body['user'];
                    let sha1sum = body['sha1sum'];

                    let query = client.query(`
                        begin;
                            insert into _user values ('${user}');
                            insert into auth_data values ('${user}', '${sha1sum}', '${auth_token}');
                        commit;
                    `);

                    query.on('error', function(err) {
                        done();
                        return fail(new Error(`db.query.failed, ${err.message}`), res);
                    });

                    query.on('end', function() {
                        done();
                        res.header('Set-Cookie', `auth_token=${auth_token}`);
                        return res.end(JSON.stringify({
                            auth_token: auth_token,
                        }));
                    });
                } else {
                    let row = rows[0];

                    if (row['sha1sum'] !== body['sha1sum']) {
                        done();
                        return fail(new Error('middleware.password.invalid'), res);
                    }

                    done();
                    res.header('Set-Cookie', `auth_token=${row['auth_token']}`);
                    return res.end(JSON.stringify({
                        auth_token: row['auth_token'],
                    }));
                }
            });
        });
    });
});

router.post('/plate', as_admin_only(function(req, res) {
    let lst = [];

    req.on('data', function(chunk) {
        lst.push(chunk);
    });

    req.on('end', function() {
        let body = {};

        try {
            body = JSON.parse(lst.join(''));
        } catch (e) {
            return fail(new Error(`middleware.malformed.request, ${e.message}`), res);
        }

        if (!body.plate || !body.plate.name || !body.plate.minutes_needed) {
            return fail(new Error('middleware.malformed.request'), res);
        }

        pg.connect(connection_string, function(err, client, done) {
            if (err) {
                done();
                return fail(new Error(`db.connection, ${err.message}`))
            }

            var p = body.plate;
            let fmt = `
                insert into
                    plate(name, img_url, description, minutes_needed, complexity)
                        values ($1, $2, $3, $4, $5)
            `;
            let query = client.query(fmt, [
                p.name, p.img_url, p.description,
                p.minutes_needed, p.complexity
            ]);

            query.on('error', function(err) {
                done();
                return fail(new Error(`db.query.failed, ${err.message}`), res);
            });

            query.on('end', function() {
                done();
                return res.end(JSON.stringify({
                    error: null,
                }));
            });
        });
    });
}));

router.get('/plate', function(req, res) {
    pg.connect(connection_string, function(err, client, done) {
        if (err) {
            done();
            return fail(new Error(`db.connection, ${err.message}`), res);
        }

        let query = client.query(`
            select p.*, at.many, at.price
            from plate as p left outer join plate_available as at
            on p.id=at.plate
        `);

        query.on('error', function(err) {
            done();
            return fail(new Error(`db.connection, ${err.message}`), res);
        });

        let rows = [];

        query.on('row', function(row) {
            rows.push(row);
        });

        query.on('end', function() {
            done();
            return res.end(JSON.stringify({
                plates: rows,
            }));
        });
    });
});

router.post('/plate_available', as_admin_only(function(req, res) {
    let lst = [];

    req.on('data', function(chunk) {
        lst.push(chunk);
    });

    req.on('end', function() {
        let body = '';

        try {
            body = JSON.parse(lst.join(''));
        } catch (e) {
            return fail(new Error(`middleware.malformed.request, ${e.message}`), res);
        }

        if (!body.plate || !body.plate.id || !body.plate.many || !body.plate.price) {
            return fail(new Error('middleware.malformed.request'), res);
        }

        pg.connect(connection_string, function(err, client, done) {
            if (err) {
                done();
                return fail(new Error(`db.connection, ${err.message}`), res);
            }

            let p = body.plate;

            let query = client.query('select * from plate_available where plate=$1', [p.id]);

            query.on('error', function(err) {
                done();
                return fail(new Error(`db.query.failed, ${err.message}`), res);
            });

            let rows = [];

            query.on('row', function(row) {
                rows.push(row);
            });

            query.on('end', function() {
                if (rows.length === 1) {
                    let query = client.query(`
                        update plate_available
                        set many=$1, price=$2 where plate=$3
                    `, [p.many, p.price, p.id]);

                    query.on('error', function(err) {
                        done();
                        return fail(new Error(`db.query.failed, ${err.message}`), res);
                    });

                    query.on('end', function() {
                        done();
                        return res.end(JSON.stringify({
                            error: null,
                        }));
                    });
                } else if (rows.length === 0) {
                    let query = client.query(`
                        insert into plate_available
                        values ($1, $2, $3)
                    `, [p.id, p.many, p.price]);

                    query.on('error', function(err) {
                        done();
                        return fail(new Error(`db.query.failed, ${err.message}`), res);
                    });

                    query.on('end', function() {
                        done();
                        return res.end(JSON.stringify({
                            error: null,
                        }));
                    });
                } else {
                    done();
                    return fail(new Error('db.inconsistency'), res);
                }
            });
       });
    });
}));

router.post('/order', function(req, res) {
    let lst = [];

    req.on('data', function(chunk) {
        lst.push(chunk);
    });

    req.on('end', function() {
        let body = ''

        try {
            body = JSON.parse(lst.join(''));
        } catch (e) {
            return fail(new Error(`middleware.malformed.request, ${e.message}`), res);
        }

        if (!body['id']) {
            return fail(new Error('middleware.malformed.request'), res);
        }

        if (!body['plates'] || !Array.isArray(body['plates'])) {
            return fail(new Error('middleware.malformed.request'), res);
        }

        pg.connect(connection_string, function(err, client, done) {
            if (err) {
                done();
                return fail(new Error(`db.connection, ${err.message}`), res);
            }

            let auth_token = '';

            try {
                auth_token = parse_cookie(req.headers['cookie'])['auth_token'];
            } catch (e) {
                done();
                return fail(new Error('middleware.permission.denied'), res);
            }

            let query = client.query('select * from auth_data where auth_token=$1', [auth_token]);

            let rows = [];

            query.on('error', function(err) {
                done();
                return fail(new Error(`middleware.permission.denied, ${err.message}`), res);
            });

            query.on('row', function(row) {
                rows.push(row);
            });

            query.on('end', function() {
                if (rows.length !== 1) {
                    done();
                    return fail(new Error('db.inconsistency'), res);
                }
                
                let user = rows[0]['_user'];

                let query = client.query(`
                    select *
                    from insert_order(($1, $2, $3, $4, $5, $6, $7, $8, $9))
                `, [
                    body['id'], user, body['last_name'],
                    body['phone_number'], body['ssn'],
                    body['vat'], body['requested_at'],
                    body['address_to_deliver'], body['plates']
                ]);

                query.on('error', function(err) {
                    done();
                    return fail(new Error(`db.query.failed, ${err.message}`), res);
                });

                rows = [];

                query.on('row', function(row) {
                    rows.push(row);
                });

                query.on('end', function() {
                    if (rows.length !== 1) {
                        done();
                        return fail(new Error('db.inconsistency'), res);
                    }

                    if (rows[0]['insert_order'] !== body['id']) {
                        done();
                        return fail(new Error('db.inconsistency'), res);
                    }

                    let query = client.query('select * from _order where id=$1', [body['id']]);

                    query.on('error', function(err) {
                        done();
                        return fail(new Error('db.query.failed'), res);
                    });

                    rows = [];

                    query.on('row', function(row) {
                        rows.push(row);
                    });

                    query.on('end', function() {
                        if (rows.length !== 1) {
                            done();
                            return fail(new Error('db.inconsistency'), res);
                        }

                        done();
                        return res.end(JSON.stringify({
                            id: rows[0]['id'],
                            requested_at: rows[0]['requested_at'],
                            proposed_at: rows[0]['proposed_at'],
                        }));
                    });
                });
            });
        });
    });
});

router.get('/order/by/:user', function(req, res) {
    pg.connect(connection_string, function(err, client, done) {
        if (err) {
            done();
            return fail(new Error(`db.connection, ${err.message}`), res);
        }

        let query = client.query(`
            select _order, last_name, o.proposed_at, name
            from _order as o
            join plate_ordered as po on po._order=o.id
            join plate as p on p.id=po.plate
            where _user=$1
        `, [req.params['user']]);

        query.on('error', function(err) {
            done();
            return fail(new Error(`db.query.failed, ${err.message}`), res);
        });

        let rows = [];

        query.on('row', function(row) {
            rows.push(row);
        });

        query.on('end', function() {
            let dict = {}

            for (let r of rows) {
                if (!dict[r['_order']]) {
                    dict[r['_order']] = {
                        last_name: r['last_name'],
                        proposed_at: r['proposed_at'],
                        plate_names: [],
                    };
                }

                dict[r['_order']]['plate_names'].push(r['name']);
            }

            let orders = [];
        
            for (let k in dict) {
                orders.push({
                    id: k,
                    last_name: dict[k]['last_name'],
                    proposed_at: dict[k]['proposed_at'],
                    plate_names: dict[k]['plate_names'],
                });
            }

            done();
            return res.end(JSON.stringify({
                orders: orders,
            }));
        });
    });
});

router.get('/stats', function(req, res) {
    pg.connect(connection_string, function(err, client, done) {
        if (err) {
            done();
            return fail(new Error(`db.connection, ${err.message}`), res);
        }

        let query = client.query('select * from nthreads');

        query.on('error', function(err) {
            done();
            return fail(new Error(`db.query.failed, ${err.message}`), res);
        });

        let rows = [];

        query.on('row', function(row) {
            rows.push(row);
        })
        
        query.on('end', function() {
            if (rows.length > 1) {
                done();
                return fail(new Error('db.inconsistency'), res);
            }

            if (rows.length === 0) {
                done();
                return res.end(JSON.stringify({stats: []}));
            }

            let nthreads = rows[0]['nthreads'];
            let counter = nthreads;

            let errors = [];

            let stats = [];

            for (let i = 0; i < nthreads; ++i) {
                let query = client.query('select * from compute_stats_for_thread($1)', [i]);

                query.on('error', function(err) {
                    errors.push(new Error(`db.query.failed, ${err.message}`));
                });


                query.on('row', function(row) {
                    stats.push(row);
                });

                query.on('end', function() {
                    counter -= 1;

                    if (counter === 0) {
                        if (stats.length !== nthreads) {
                            errors.push(new Error('db.inconsistency'));
                        }

                        if (errors.length > 0) {
                            done();
                            return fail(errors[0], res);
                        }
                        
                        done();
                        return res.end(JSON.stringify({
                            stats: stats,
                        }));
                    }
                });
            }
        });
    });
});

router.delete('/order/:id', function(req, res) {
    pg.connect(connection_string, function(err, client, done) {
        if (err) {
            done();
            return fail(new Error(`db.connection, ${err.message}`), res);
        }

        let query = client.query('select * from _order where id=$1', [req.params['id']]);

        query.on('error', function(err) {
            done();
            return fail(new Error(`db.query.failed, ${err.message}`), res);
        });

        let rows = [];

        query.on('row', function(row) {
            rows.push(row);
        });

        query.on('end', function() {
            if (rows.lenght > 1) {
                done();
                return fail(new Error('db.inconsistency'), res);
            }

            if (rows.length === 0) {
                done();
                return fail(new Error('db.item.not_existing'), res);
            }

            let user = rows[0]['_user'];

            let query = client.query('select * from auth_data where _user=$1', [user]);

            query.on('error', function(err) {
                done();
                return fail(new Error(`db.query.failed, ${err.message}`), res);
            });

            rows = [];

            query.on('row', function(row) {
                rows.push(row);
            });

            query.on('end', function() {
                if (rows.length !== 1) {
                    done();
                    return fail(new Error('db.inconsistency'), res);
                }

                let auth_token = '';
                try {
                    auth_token = parse_cookie(req.headers['cookie'])['auth_token'];
                } catch (e) {
                    done();
                    return fail(new Error('middleware.permission.denied'), res);
                }

                let query = client.query('select * from auth_data where auth_token=$1', [auth_token]);

                query.on('error', function(err) {
                    done();
                    return fail(new Error(`db.query.failed, ${err.message}`), res);
                });

                rows = [];

                query.on('row', function(row) {
                    rows.push(row);
                });

                query.on('end', function() {
                    if (rows.length > 1) {
                        done();
                        return fail(new Error('db.inconsistency'), res);
                    }

                    if (rows.length === 0) {
                        done();
                        return fail(new Error('middleware.permission.denied'), res);
                    }

                    if (rows[0]['_user'] !== user) {
                        done();
                        return fail(new Error('middleware.permission.denied'), res);
                    }

                    let query = client.query('delete from _order where id=$1', [req.params['id']]);

                    query.on('error', function(err) {
                        done();
                        return fail(new Error(`db.query.failed, ${err.message}`), res);
                    });

                    query.on('end', function() {
                        done();
                        return res.end(JSON.stringify({
                            error: null,
                        }));
                    })
                });
            });
        });
    });
});

router.get('/whoami', function(req, res) {
    let auth_token = null;
    try {
        auth_token = parse_cookie(req.headers['cookie'])['auth_token'];
    } catch (e) { ; }

    if (!auth_token) {
        return fail(new Error('middleware.permission.denied'), res);
    }

    pg.connect(connection_string, function(err, client, done) {
        if (err) {
            done();
            return fail(new Error(`db.connection, ${err.message}`), res);
        }

        let query = client.query('select * from auth_data where auth_token=$1', [auth_token]);

        query.on('error', function(err) {
            done();
            return fail(new Error(`db.query.failed, ${err.message}`), res);
        });

        let rows = [];

        query.on('row', function(row) {
            rows.push(row);
        });

        query.on('end', function() {
            if (rows.length === 0) {
                done();
                return fail(new Error('middleware.permission.denied'), res);
            }

            if (rows.length > 1) {
                done();
                return fail(new Error('db.inconsistency'), res);
            }

            done();
            return res.end(JSON.stringify({
                user: rows[0]['_user'],
            }));
        });
    });
});

module.exports = router;
