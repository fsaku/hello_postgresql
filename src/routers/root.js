'use strict';

const express = require('express');
const router = express.Router();
const fs = require('fs');
const pg = require('pg');

const parse_cookie = require('../../lib/parse_cookie');

const connection_string = 'postgres://localhost:5432/test';

router.get('/', onRoot);

router.get('/auth', onLogin); 

router.get('/admin', onAdmin); 

router.get('/user', onUser);

router.get('/change_passwd', onChangePasswd);

router.get('/utility.js', function(req, res) {
    fs.readFile('./src/views/utility.js', 'UTF-8', function(err, js) {
        if (err) {
            return res.end('Error while fetching utility.js', res);
        }

        return res.end(js);
    });
});

router.get('/style.css', function(req, res) {
    fs.readFile('./src/views/style.css', 'UTF-8', function(err, css) {
        if (err) {
            return res.end('Error while fetching style.css', res);
        }

        return res.end(css);
    });
});

function
onRoot(req, res) {
    let auth_token = null;
    try {
        auth_token = parse_cookie(req.headers.cookie).auth_token;
    } catch (e) { ; }

    if (!auth_token) {
        return onLogin(req, res);
    }

    pg.connect(connection_string, function(err, client, done) {
        if (err) {
            done();
            res.end('DB failed to connect. Retry later.');
        }

        let query = client.query('select * from auth_data where auth_token=$1', [auth_token]);

        query.on('error', function(err) {
            done();
            res.end('DB query failed. Retry later.');
        });

        let rows = [];

        query.on('row', function(row) {
            rows.push(row);
        });

        query.on('end', function() {
            done();
            if (rows.length > 1) {
                res.end('DB has inconsistent state. Retry later.');
            } else if (rows.length > 0 && rows[0]._user === 'admin') {
                return onAdmin(req, res);
            } else if (rows.length > 0) {
                return onUser(req, res);
            } else {
                return onLogin(req, res);
            }
        });
    });
}

function
onLogin(req, res) {
    return fs.readFile('./src/views/auth.html', 'UTF-8', function(err, html) {
        if (err) {
            console.error(err);
            return res.end('Failed while fetching the page. Retry later.');
        }

        return res.end(html);
    });
}

function
onChangePasswd(req, res) {
    return fs.readFile('./src/views/change_passwd.html', 'UTF-8', function(err, html) {
        if (err) {
            console.error(err);
            return res.end('Failed while fetching the page. Retry later.');
        }

        return res.end(html);
    });
}

function
onAdmin(req, res) {
    return fs.readFile('./src/views/admin.html', 'UTF-8', function(err, html) {
        if (err) {
            console.error(err);
            return res.end('Failed while fetching the page. Retry later.');
        }

        return res.end(html);
    });
}

function
onUser(req, res) {
    return fs.readFile('./src/views/user.html', 'UTF-8', function(err, html) {
        if (err) {
            console.error(err);
            return res.end('Failed while fetching the page. Retry later.');
        }

        return res.end(html);
    });
}

module.exports = router;
